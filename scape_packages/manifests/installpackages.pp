/*
Copyright 2014 Universitatea de Vest din Timișoara

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author: Caius Bogdanescu <caius.bogdanescu@info.uvt.ro>
@contact: caius.bogdanescu@info.uvt.ro
@copyright: 2014 Universitatea de Vest din Timișoara
*/

# Class: scape_packages::installpackages
#
# This module installs the SCAPE packages 
#
class scape_packages::installpackages{

 #SCAPE jpylyzer package
 package { "jpylyzer":
 ensure => "latest",
 require => [Apt::Source['OPF'], Apt::Source['BinTray'],File['99unauth']]
 }

 #SCAPE pagelyzer package
 package { "pagelyzer-ruby":
 ensure => "latest",
 require => [Apt::Source['OPF'], Apt::Source['BinTray'], Package['jpylyzer']]
 }

 #SCAPE xcorrsound package
 package { "scape-xcorrsound":
 ensure => "latest",
 require => [Apt::Source['OPF'], Apt::Source['BinTray'], Package['pagelyzer-ruby']]
 }

 package { "imagemagick":
	ensure => "latest",	
 }

 package { "openjpeg-tools":
	ensure => "latest"
 }

}


