class scape_packages::hadoop{
      cdh4::hadoop::directory { '/user/scape':
          owner   => 'ubuntu',
	  group   => 'hdfs',
	  mode => "1755",
	  require => [Cdh4::Hadoop::Directory['/user']],
      }

file {'/usr/local/scape':
     ensure => "directory"
}

file {'/usr/local/scape/toolspecs/':
     ensure => "directory",
     require => [File["/usr/local/scape"]]
}


file { 'digital-preservation-characterisation-image-jp2-jpylyzer.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-characterisation-image-jp2-jpylyzer.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-characterisation-image-jp2-jpylyzer.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-characterisation-tika-app-parse2text.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-characterisation-tika-app-parse2text.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-characterisation-tika-app-parse2text.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-characterisation-tika-app-wrapper.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-characterisation-tika-app-wrapper.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-characterisation-tika-app-wrapper.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-characterisation-video-ffprobe-video2xml.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-characterisation-video-ffprobe-video2xml.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-characterisation-video-ffprobe-video2xml.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-identification-droid-folder2csv.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-identification-droid-folder2csv.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-identification-droid-folder2csv.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-audio-ffmpeg-audio2aac.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-audio-ffmpeg-audio2aac.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-audio-ffmpeg-audio2aac.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-audio-ffmpeg-audio2mp3.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-audio-ffmpeg-audio2mp3.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-audio-ffmpeg-audio2mp3.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-audio-ffmpeg-mp32wav.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-audio-ffmpeg-mp32wav.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-audio-ffmpeg-mp32wav.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-audio-gstreamer-mp32flac.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-audio-gstreamer-mp32flac.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-audio-gstreamer-mp32flac.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-audio-gstreamer-mp32ogg.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-audio-gstreamer-mp32ogg.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-audio-gstreamer-mp32ogg.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-audio-gstreamer-mp32wav.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-audio-gstreamer-mp32wav.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-audio-gstreamer-mp32wav.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-audio-mpg321-mp32wav.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-audio-mpg321-mp32wav.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-audio-mpg321-mp32wav.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-audio-sox-audio2wav.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-audio-sox-audio2wav.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-audio-sox-audio2wav.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-audio-sox-mp32wav.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-audio-sox-mp32wav.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-audio-sox-mp32wav.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-image-ffmpeg-image2tiff.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-image-ffmpeg-image2tiff.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-image-ffmpeg-image2tiff.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-image-gimp-png2tiff.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-image-gimp-png2tiff.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-image-gimp-png2tiff.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-image-gimp-tiff2jpg.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-image-gimp-tiff2jpg.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-image-gimp-tiff2jpg.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-image-gimp-tiff2png.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-image-gimp-tiff2png.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-image-gimp-tiff2png.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-image-graphicsmagick-image2tiff.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-image-graphicsmagick-image2tiff.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-image-graphicsmagick-image2tiff.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-image-graphicsmagick-tiff2jp2.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-image-graphicsmagick-tiff2jp2.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-image-graphicsmagick-tiff2jp2.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-image-graphicsmagick-tiff2png.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-image-graphicsmagick-tiff2png.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-image-graphicsmagick-tiff2png.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-image-imageio-image2bmp.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-image-imageio-image2bmp.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-image-imageio-image2bmp.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-image-imageio-image2gif.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-image-imageio-image2gif.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-image-imageio-image2gif.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-image-imageio-image2jpeg.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-image-imageio-image2jpeg.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-image-imageio-image2jpeg.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-image-imagemagick-html2jp2.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-image-imagemagick-html2jp2.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-image-imagemagick-html2jp2.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-image-imagemagick-html2tiff.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-image-imagemagick-html2tiff.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-image-imagemagick-html2tiff.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-image-imagemagick-html2txt.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-image-imagemagick-html2txt.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-image-imagemagick-html2txt.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-image-imagemagick-image2jp2.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-image-imagemagick-image2jp2.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-image-imagemagick-image2jp2.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-image-imagemagick-image2tiff.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-image-imagemagick-image2tiff.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-image-imagemagick-image2tiff.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-image-imagemagick-image2txt.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-image-imagemagick-image2txt.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-image-imagemagick-image2txt.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-image-imagemagick-txt2jp2.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-image-imagemagick-txt2jp2.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-image-imagemagick-txt2jp2.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-image-imagemagick-txt2tiff.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-image-imagemagick-txt2tiff.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-image-imagemagick-txt2tiff.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-image-inkscape-svg2pdf.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-image-inkscape-svg2pdf.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-image-inkscape-svg2pdf.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-image-inkscape-svg2png.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-image-inkscape-svg2png.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-image-inkscape-svg2png.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-image-jasper-jp22jpg.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-image-jasper-jp22jpg.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-image-jasper-jp22jpg.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-image-netpbm-tiff2jpg.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-image-netpbm-tiff2jpg.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-image-netpbm-tiff2jpg.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-image-netpbm-tiff2png.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-image-netpbm-tiff2png.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-image-netpbm-tiff2png.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-image-openjpeg-image2jp2.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-image-openjpeg-image2jp2.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-image-openjpeg-image2jp2.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-image-sanselan-image2bmp.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-image-sanselan-image2bmp.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-image-sanselan-image2bmp.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-image-sanselan-image2png.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-image-sanselan-image2png.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-image-sanselan-image2png.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-image-tesseract-tif2txt.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-image-tesseract-tif2txt.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-image-tesseract-tif2txt.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-abiword-doc2html.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-abiword-doc2html.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-abiword-doc2html.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-abiword-doc2odt.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-abiword-doc2odt.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-abiword-doc2odt.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-abiword-doc2pdf.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-abiword-doc2pdf.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-abiword-doc2pdf.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-abiword-doc2txt.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-abiword-doc2txt.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-abiword-doc2txt.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-abiword-docx2doc.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-abiword-docx2doc.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-abiword-docx2doc.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-abiword-docx2html.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-abiword-docx2html.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-abiword-docx2html.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-abiword-docx2odt.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-abiword-docx2odt.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-abiword-docx2odt.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-abiword-docx2pdf.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-abiword-docx2pdf.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-abiword-docx2pdf.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-abiword-docx2txt.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-abiword-docx2txt.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-abiword-docx2txt.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-ffmpeg-txt2tiff.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-ffmpeg-txt2tiff.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-ffmpeg-txt2tiff.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-jodconverter-doc2odt.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-jodconverter-doc2odt.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-jodconverter-doc2odt.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-jodconverter-doc2pdf.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-jodconverter-doc2pdf.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-jodconverter-doc2pdf.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-jodconverter-odp2pdf.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-jodconverter-odp2pdf.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-jodconverter-odp2pdf.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-jodconverter-odp2ppt.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-jodconverter-odp2ppt.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-jodconverter-odp2ppt.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-jodconverter-odp2swf.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-jodconverter-odp2swf.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-jodconverter-odp2swf.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-jodconverter-ods2pdf.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-jodconverter-ods2pdf.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-jodconverter-ods2pdf.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-jodconverter-ods2xls.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-jodconverter-ods2xls.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-jodconverter-ods2xls.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-jodconverter-odt2doc.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-jodconverter-odt2doc.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-jodconverter-odt2doc.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-jodconverter-odt2pdf.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-jodconverter-odt2pdf.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-jodconverter-odt2pdf.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-jodconverter-ppt2odp.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-jodconverter-ppt2odp.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-jodconverter-ppt2odp.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-jodconverter-ppt2pdf.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-jodconverter-ppt2pdf.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-jodconverter-ppt2pdf.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-jodconverter-ppt2swf.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-jodconverter-ppt2swf.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-jodconverter-ppt2swf.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-jodconverter-pptx2odp.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-jodconverter-pptx2odp.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-jodconverter-pptx2odp.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-jodconverter-pptx2pdf.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-jodconverter-pptx2pdf.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-jodconverter-pptx2pdf.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-jodconverter-pptx2swf.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-jodconverter-pptx2swf.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-jodconverter-pptx2swf.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-jodconverter-xls2ods.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-jodconverter-xls2ods.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-jodconverter-xls2ods.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-jodconverter-xls2pdf.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-jodconverter-xls2pdf.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-jodconverter-xls2pdf.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-pdfbox-pdf2txt.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-pdfbox-pdf2txt.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-pdfbox-pdf2txt.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-office-pdfbox-txt2pdf.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-office-pdfbox-txt2pdf.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-office-pdfbox-txt2pdf.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-video-avidemux-flv2avi.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-video-avidemux-flv2avi.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-video-avidemux-flv2avi.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-video-avidemux-wmv2mpeg2.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-video-avidemux-wmv2mpeg2.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-video-avidemux-wmv2mpeg2.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-video-ffmpeg-video2flv.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-video-ffmpeg-video2flv.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-video-ffmpeg-video2flv.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-video-ffmpeg-video2mpeg2.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-video-ffmpeg-video2mpeg2.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-video-ffmpeg-video2mpeg2.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-video-handbrake-video2mpeg4.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-video-handbrake-video2mpeg4.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-video-handbrake-video2mpeg4.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-migration-video-mencoder-video2mpeg2.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-migration-video-mencoder-video2mpeg2.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-migration-video-mencoder-video2mpeg2.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-qaobject-audio-xcorrsound-comparison.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-qaobject-audio-xcorrsound-comparison.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-qaobject-audio-xcorrsound-comparison.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}
file { 'digital-preservation-qa-web-pagelyzer-changedetection.xml':
     ensure=>'present',
     path=>'/usr/local/scape/toolspecs/digital-preservation-qa-web-pagelyzer-changedetection.xml',
     source=>'puppet:///modules/scape_packages/digital-preservation-qa-web-pagelyzer-changedetection.xml',
     require => [File["/usr/local/scape/toolspecs"]]
}

}