node scapecdh {

  class { 'apt':
    disable_keys => true,
  }

  apt::source { 'clouder-cdh4-repository':
    location     => 'http://ftp.info.uvt.ro/pub/cloudera/mirror/mirror/archive.cloudera.com/cdh4/ubuntu/precise/amd64/cdh/',
    release      => 'precise-cdh4',
    repos => 'contrib',
    architecture => 'amd64',
    key => '02A818DD',
    key_server => 'keys.gnupg.net',
    include_src  => false,
  }


  file { '/var/lib/hadoop/':
    ensure => "directory",
    owner => "hdfs"
  }

  file { '/var/lib/hadoop/data/':
    ensure => "directory",
    owner => "hdfs",
    require => File["/var/lib/hadoop/"]
  }

  file { '/usr/lib/hadoop-hdfs/libexec':
    ensure => "link",
    target => "/usr/lib/hadoop/libexec/"
  }

  file {"/usr/lib/hadoop/logs":
    ensure => "directory",
    owner => "hdfs"
  }

  class {"java":
    version => "java6"
  }

  class { 'cdh4::hadoop':
    # Must pass an array of hosts here, even if you are
    # not using HA and only have a single NameNode.
    namenode_hosts     => ['scape-hadoop-namenode.local'],
    datanode_mounts    => [
                           '/var/lib/hadoop/data/a',
                           '/var/lib/hadoop/data/b',
                           '/var/lib/hadoop/data/c'
                           ],
    # You can also provide an array of dfs_name_dirs.
    dfs_name_dir       => '/var/lib/hadoop/name',
    notify => [File["/var/lib/hadoop/"], File["/usr/lib/hadoop-hdfs/libexec"], File["/usr/lib/hadoop/logs/"]],
    require => [Class["java"],  Apt::Source["clouder-cdh4-repository"]]
  }

  class { 'cdh4::pig':
    require => [Class["cdh4::hadoop"],]
  }
  
  Host <<| |>>


  
  # Scape related
  class{'scape_packages':}
  class{'cdh4::oozie':}
}

node hadoop_server inherits scapecdh {

  include cdh4::hadoop::master
  #class { 'cdh4::hive::master': }
  @@host { "$ec2_hostname":
    ip => "$ec2_local_ipv4",
    host_aliases => ["$hostname", "scape-hadoop-namenode.local"],
  }

  cdh4::hadoop::directory { '/user/ubuntu':
        owner   => 'ubuntu',
        group   => 'hdfs',
        mode => "1755",
        require => [Cdh4::Hadoop::Directory['/user']],
  }

  class{'scape_packages::hadoop':}

  package {"hadoop-hdfs-fuse":
    ensure => "present",
    require => [Class["java"],  Apt::Source["clouder-cdh4-repository"]]
  }
  
  file {"/export":
    ensure => "directory"
  }
  
  file {"/export/hdfs":
    ensure => "directory",
    require => [File["/export"], Apt::Source["clouder-cdh4-repository"]]
  }

  file {"/etc/default/hadoop-fuse":
    ensure => "present",
    mode => "0644",
    content => "export LIBHDFS_OPTS=\"-Xms128m -Xmx512m\"\nexport JAVA_HOME=/usr/lib/jvm/java-6-oracle/jre"
  }
  
  mount { "hdfs_mountpoint":
    name => "/export/hdfs",
    ensure => "mounted",
    atboot => false,
    device => "hadoop-fuse-dfs#dfs://${ec2_local_hostname}:8020",
    fstype => "fuse",
    options => "allow_other,usetrash,rw",
    require => [Package["hadoop-hdfs-fuse"], File["/export/hdfs"], Service['hadoop-hdfs-namenode'], File["/etc/default/hadoop-fuse"]]
  }

  class { 'motd':
      template => 'scape_packages/motd.erb',
  }


  package {'mysql-server':
    ensure => "installed"
  }
  class { 'cdh4::oozie::server':
    jdbc_password => 'scape14',
    require => [Package["mysql-server"], ]
  }

  class { 'cdh4::hue':
    secret_key => "scape2014vienna",
  }

  file { '/usr/share/hue':
    ensure => "directory",
    owner => "hue",
    group => "hue",
    recurse => true,
    notify => [Service['hue'],],
  }

  file { '/etc/ssl/private':
    ensure => "directory",
    owner => "root",
    mode => "0711",
    notify => [Service['hue'],]
  }
}

node hadoop_worker inherits scapecdh {
  include cdh4::hadoop::worker
  @@host { "$ec2_hostname":
    ip => "$ec2_local_ipv4",
    host_aliases => "$hostname",
  }
}
