/*
Copyright 2014 Universitatea de Vest din Timișoara

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author: Caius Bogdanescu <caius.bogdanescu@info.uvt.ro>
@contact: caius.bogdanescu@info.uvt.ro
@copyright: 2014 Universitatea de Vest din Timișoara
*/

# Class: scape_taverna::install
#
# This class installs Taverna Server
#

class scape_taverna::install{

  class{'scape_tomcat':
  port => $scape_taverna::tomcatPort
  }

  user { "add-taverna-user":
    name   => "taverna",
    ensure => present,
  }
  
  exec { "download-taverna-server":
    command => "wget -O /mnt/taverna_server.war ${scape_taverna::tavernaLocation}",
    path    => '/usr/bin',
    onlyif  => "test ! -e /mnt/taverna_server.war"
  }

  scape_tomcat::deploywebapp {'taverna':
    appname     => taverna,
    source      => "/mnt/taverna_server.war",
    destination => "${scape_taverna::tomcatWebApps}/taverna.war",
    require     => File['apply-tomcat-server-template'],
  }
}
