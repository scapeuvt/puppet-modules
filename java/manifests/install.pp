class java::install  inherits java::params {
  
  if $::osfamily == Debian {
    
#     file { 'sources.list.d': 
#    	 path => '/etc/apt/sources.list.d/',
#	 ensure => directory,
#    }
    
    apt::key { 'webupd8team':
      key        => 'EEA14886',
      key_server => 'keyserver.ubuntu.com',
      require => File['sources.list.d'],
    }

    
    #add repose with oracle java packages to sourse.list
    apt::ppa { 'http://ppa.launchpad.net/webupd8team/java/ubuntu':
      require  => Apt::Key['webupd8team'],
    }
    
    # set licese for Oracle Java
    exec {'set-licence-seen':
      command => '/bin/echo debconf shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections',
      onlyif  => '/usr/bin/debconf-get-selections | /bin/grep "accepted-oracle-license-v1-1"|/bin/grep "true"|/usr/bin/wc -l|/bin/grep 0',
      require => Apt::Ppa['http://ppa.launchpad.net/webupd8team/java/ubuntu'],
    }
    
    package { 'java':
      name    => $java_package,
      ensure  => "installed",
      require => [Exec['set-licence-seen'], File["/usr/libexec/bigtop-detect-javahome"]]
    }

    file {"/etc/profile.d/99_java.sh":
      ensure => "present",
      content => "export JAVA_HOME=/usr",
      require => Package["java"]
    }
    
    file { "/usr/libexec":
    	 ensure => "directory"
    }	 

    file { "/usr/libexec/bigtop-detect-javahome":
    	 ensure => "present",
	 content => "export JAVA_HOME=/usr",
	 require => File["/usr/libexec/"]
    }   	 
  }
}
