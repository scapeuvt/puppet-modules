class java::params{
  case $::osfamily {
    
    Debian: {
      if $java::version == 'java6'{
        $java_package=['oracle-java6-installer']
        $java_destination='/usr/lib/jvm/java-6-oracle/jre'
      }
      if $java::version == 'java7'{
        $java_package=['oracle-java7-installer']
        $java_destination='/usr/lib/jvm/java-7-oracle/jre'
      }
    }
    
    default: {
      err("Currently this module only supports Ubuntu/Debian")
    } 
  } 
}
