/*
Copyright 2014 Universitatea de Vest din Timișoara

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author: Caius Bogdanescu <caius.bogdanescu@info.uvt.ro>
@contact: caius.bogdanescu@info.uvt.ro
@copyright: 2014 Universitatea de Vest din Timișoara
*/

# Class: scape_tomcat::configure
#
# This class configures Apache Tomcat 7
#

class scape_tomcat::configure{

  exec { "createKey":
    command => "keytool -alias tomcatKey -genkey -keystore /mnt/tomcatKey -storepass tomcat -keypass tomcat -keyalg RSA -dname \"CN=Caius Bogdanescu, OU=UVT, O=UVT, L=Timisoara, S=Timis, C=Romania\" ",
    path    => "/usr/sbin:/usr/bin:/bin",
    onlyif  => "test ! -f /mnt/tomcatKey",
    before  => Exec["make-tomcat-user-passwordless"],
  }

  exec { "make-tomcat-user-passwordless":
    command => "echo 'tomcat7 ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers.d/90-cloudimg-ubuntu",
    path    => "/usr/sbin:/usr/bin:/bin",
    unless  => "cat /etc/sudoers.d/90-cloudimg-ubuntu | grep 'tomcat7 ALL=(ALL) NOPASSWD:ALL'",
    require => Package['tomcat7-admin']
  }

  exec { "configure-tomcat-jdk":
    command => "echo 'JAVA_HOME=${scape_tomcat::jdk}' >> /etc/default/tomcat7",
    path    => "/usr/sbin:/usr/bin:/bin",
    unless  => "cat /etc/default/tomcat7 | grep JAVA_HOME=${scape_tomcat::jdk}",
    require => Exec["make-tomcat-user-passwordless"],
  }

  exec { "configure-tomcat-jmx":
    command => "echo 'export CATALINA_OPTS=-Dcom.sun.management.jmxremote' >> /usr/share/tomcat7/bin/startup.sh",
    path    => "/usr/sbin:/usr/bin:/bin",
    unless  => "cat /usr/share/tomcat7/bin/startup.sh | grep 'export CATALINA_OPTS=-Dcom.sun.management.jmxremote'",
    require => Exec["configure-tomcat-jdk"]
  }

  file { "apply-tomcat-users-template":
    ensure   => present,
    path     => "/etc/tomcat7/tomcat-users.xml",
    content  => template("scape_tomcat/tomcat-users.xml.erb"),
    require  => Exec["configure-tomcat-jmx"]
  }

  file { "apply-tomcat-server-template":
    ensure   => present,
    path     => "/etc/tomcat7/server.xml",
    content  => template("scape_tomcat/tomcat-server.xml.erb"),
    require  => File["apply-tomcat-users-template"],
  
  }

}
