# = Class: s3fsfuse
#
# Author: Adrian Spataru <spataru.florin@info.uvt.ro>

class s3fsfuse {
    $fuse = '1.78'
    package {
        'build-essential':
            ensure      => installed,
            provider    => apt,
    }

    package {
        'libfuse-dev':
            ensure      => installed,
            provider    => apt,
    }

    package {
        'fuse-utils':
            ensure      => installed,
            provider    => apt,
    }

    package {
        'libcurl4-openssl-dev':
            ensure      => installed,
            provider    => apt,
    }

    package {
        'libxml2-dev':
            ensure      => installed,
            provider    => apt,
    }

    package {
        'mime-support':
            ensure      => installed,
            provider    => apt,
    }

    package {
        'automake':
            ensure      => installed,
            provider    => apt,
    }

    package {
        'libtool':
            ensure      => installed,
            provider    => apt,
    }

    exec {
        'download-s3fsfuse':
            command     => "wget -O /tmp/s3fs-fuse-${fuse}.tar.gz https://github.com/s3fs-fuse/s3fs-fuse/archive/v${fuse}.tar.gz",
            logoutput   => on_failure,
            onlyif      => 'test ! -e /tmp/s3fs-fuse-1.78.tar.gz',
            path   => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
            #refreshonly => true
    }

    exec {
        'extract-s3fsfuse':
            command     => "tar xzf s3fs-fuse-${fuse}.tar.gz",
            logoutput   => on_failure,
            path   => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
	    require => Exec['download-s3fsfuse'],
	    cwd => "/tmp",
            #refreshonly => true,
    }

    exec {
        'autogen-s3fsfuse':
            command     => 'bash autogen.sh',
            logoutput   => on_failure,
            path   => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
	    require => Exec['extract-s3fsfuse'],
            cwd => "/tmp/s3fs-fuse-${fuse}"
    }

    exec {
        'configure-s3fsfuse':
            command     => 'bash configure --prefix=/usr --with-openssl',
            logoutput   => on_failure,
            path   => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
            cwd => "/tmp/s3fs-fuse-${fuse}",
            require => Exec['autogen-s3fsfuse']

    }

    exec {
        'make-s3fsfuse':
            command     => 'make',
            logoutput   => on_failure,
            path   => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
            cwd => "/tmp/s3fs-fuse-${fuse}",
            require => Exec['configure-s3fsfuse'],
    }

    exec {
        'install-s3fsfuse':
            command     => 'make install',
            logoutput   => on_failure,
            path   => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
            cwd => "/tmp/s3fs-fuse-${fuse}",
            require => Exec['make-s3fsfuse'],
    }
}

include s3fsfuse
